list_max([X],X).
list_max([Head|Tail], Max) :-
     list_max(Tail, TailMax), Max is max(TailMax, Head).
