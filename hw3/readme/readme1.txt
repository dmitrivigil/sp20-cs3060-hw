dmitri@dmitri-VirtualBox:~$ cd Desktop
dmitri@dmitri-VirtualBox:~/Desktop$ prolog 
Welcome to SWI-Prolog (threaded, 64 bits, version 8.0.3)
SWI-Prolog comes with ABSOLUTELY NO WARRANTY. This is free software.
Please run ?- license. for legal details.

For online help and background, visit http://www.swi-prolog.org
For built-in help, use ?- help(Topic). or ?- apropos(Word).

?- [task1].
true.

?- mother(X,Y) .
X = becky,
Y = steve ;
X = becky,
Y = tim ;
X = marie,
Y = debbie ;
X = marie,
Y = david.

?- father(X,Y) .
X = jack,
Y = steve ;
X = jack,
Y = tim ;
X = david,
Y = debbie ;
X = Y, Y = david.

?- parent (david, Y) .
ERROR: Syntax error: Operator expected
ERROR: parent
ERROR: ** here **
ERROR:  (david, Y) . 
?- parent(david, Y).
Y = debbie ;
Y = david ;
false.

?- grandparent(X,Y) .
ERROR: Undefined procedure: grandparent/2 (DWIM could not correct goal)
?- grandfather(X,Y)
|    ;
|    ;
|    ;
|    ;
|    ;
|    ;
|    ;
|    .
ERROR: Syntax error: Unexpected `;' before `.'
ERROR: grandfather(X,Y)
;
;
;
;
;
;
;
ERROR: ** here **
ERROR:  . 
?- grandfather(X,Y).                                                           
ERROR: Undefined procedure: grandfather/2
ERROR:     However, there are definitions for:
ERROR:         grandfather/0
false.

?- grandfather(X,steve).
ERROR: Undefined procedure: grandfather/2
ERROR:     However, there are definitions for:
ERROR:         grandfather/0
false.

?- grandfather(jack,Y).
ERROR: Undefined procedure: grandfather/2
ERROR:     However, there are definitions for:
ERROR:         grandfather/0
false.

?- parent(jack,steve) .
true ;
false.

?- parent (david, steve) .
ERROR: Syntax error: Operator expected
ERROR: parent
ERROR: ** here **
ERROR:  (david, steve) . 
?- parent(david, steve).
false.

?- 

