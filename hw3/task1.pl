father(steve,josh).
father(steve,danielle).
father(tim,nick).
father(tim,mary).
mother(michelle,josh).
mother(debbie,danielle).
mother(katie,mary).

parent(X,Y) :- father(X,Y).
parent(X,Y) :- mother(X,Y).
grandparent(X,Y) :- parent (X,Y), parent(X,Y).


