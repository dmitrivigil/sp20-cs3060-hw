myDelete (A, [A], []) .
myDelete (A, [A|List1], List1) . 
myDelete (A, [B|List2], [B|List2]) :- myDelete(A, List2, List1) .

writeToFile(N, File) :-
	write('Enter the file name: '), nl,
	read(File), open(File, write, Stream),
	myDelete(), write(Stream, F),
	close(Stream).