#task 2
#The Goal of this task is to see what .each, .each_slice, .select, .map, .inject can do for an array
def task2
  a = []
  i = 0
  until i>49 do
    a[i] = rand(10...100)
    i += 1
  end

  a.each {|n| puts "#{n*n*n}"}
  a.each_slice(3){|n| puts n}
  a.select! {|n| n % 5 == 0}
  a.map {|n| n*n*n}

end

task2