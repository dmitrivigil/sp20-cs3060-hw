the goal of task 1 is to have objects written into a text file
the goal of task 2 is to see if the word scala is in a text file
the goal of task 3 is to cube a number 
the goal of task 4 is to use the map fuction see staticitst.

login as: dvigil
dvigil@csvm14.cs.bgsu.edu's password:
Welcome to Ubuntu 18.04.4 LTS (GNU/Linux 4.15.0-91-generic x86_64)

7 packages can be updated.
0 updates are security updates.

Last login: Sun Apr  5 18:10:18 2020 from 64.134.180.93
dvigil@csvm14:~$ cd hw5
dvigil@csvm14:~/hw5$ ls
myText.txt  task1.scala  task3.scala  test1.txt
story.txt   task2.scala  task4.scala  test2.txt
dvigil@csvm14:~/hw5$ scala task1.scala
warning: there was one deprecation warning (since 2.11.0); re-run with -deprecation for details
one warning found
enter an input
sadjkfas;l
enter an input
asdfadsg
enter an input
dgasfg
dvigil@csvm14:~/hw5$ scala task2.scala
warning: there was one deprecation warning (since 2.11.0); re-run with -deprecation for details
one warning found
enter the file name:
test2.txt
This file is meaningless
dvigil@csvm14:~/hw5$ scala task3.scala
The cube of 5 is 125
The cube of 6 is 216
The cube of 7 is 343
The cube of 8 is 512
The cube of 9 is 729
The cube of 10 is 1000
The cube of 11 is 1331
The cube of 12 is 1728
The cube of 13 is 2197
The cube of 14 is 2744
The cube of 15 is 3375
The cube of 16 is 4096
The cube of 17 is 4913
The cube of 18 is 5832
The cube of 19 is 6859
The cube of 20 is 8000
dvigil@csvm14:~/hw5$ scala task4.scala
69
dvigil@csvm14:~/hw5$
